# IAAS
Infrastructure-as-a-spec

## EXPLORE

- [Project repository]:(https://github.com/p2pcloud)
- [Cloud workspaces]:(http://codenvy.io)

## DESIGN

### FEATURES
- Ubuntu Mate Desktop
- TightVNC service
- NoVNC web service
- CloudFoundry CLI
### REQUIRES
- 
### BLOCKING
- Mail transfer agent (incoming+outgoing) using a custom domainname. 
- Issue #4 requires the execution of an action at workspace load event to aquire the dynamic hostname.
### SOLUTION
- 
### OPTIMIZE
- [Distributed model] stores metadata for external system's streaming data.
- [Immutable models] entirely expressed with non-conflicting graph models.
- [Cache optimization] maximizes cache hits and allows append-only storage.
- [Replication partner] selected by using the connectivity graph's "cut edges".
- [Stream transformation] becomes the first-class citizen of any exchange.
- [Smart-contract] stream using proof-of-work and token payments.
- [Zero-knowledge] mining peers executes and acts on behalf of other peers.
- [Control plane] responsibilities are randomly switched between mining peers.
- [Agent-based work] fulfilled by randomly choosen zero-knowledge miner peers.
- [Transport-agnostic] transfer components allows many types of network connections.
- [Undisclosed endpoints] allows no mass-exposure of real physical network interface addresses.
- [Control-Flow-Graph] express the semantic of distributed apps.
- [Data-locality aware] processing reduces data access times.
- [Functional model] express all model changes using programmatic operations.
- [Function composition] allows results to be expressed prior to calculation. 
- [Function pipelines] scale distributed processing by decoupling data and processing.
- [Reducible pipelines] predict processing of coherent data transformations.
- [Pre-defined enumerator] allows optimization of indexed data.
- [Cryptographic tokens] establish a fair-trade of virtual ressources.
- [Automated brokerage] exchanges tokens for price-finding and anonymity.
- [Coorporative streaming] maximizes the direct edge-to-edge bandwidth.
 
## DEPLOYMENT

### INSTALLATION

1. Clone the infrastructure repository.
`` sh
git clone git+https://github.com/p2pcloud/iaas
``

### CONTRIBUTION

### INTEGRATIONS
- [ ] Cloud-based repository provided by [GITHUB.COM]:(https://github.com/p2pcloud) at no cost.
- [ ] Cloud-based workspace provided by [CODENVY.IO]:(https://codenvy.io) at no cost.
- [ ] Mobile device shell provided by [TERMUX] at no cost.
- [ ] Registered domainname [PEER2PEER.CLOUD] provided by [ARUBA.CLOUD] at $1/yr.
