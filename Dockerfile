FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install --no-install-recommends --yes ubuntu-mate-core
RUN apt-get install --yes git sudo vim command-not-found tightvncserver websockify
RUN git clone https://github.com/novnc/novnc /opt/novnc
RUN adduser --disabled-password --gecos '' user && adduser user sudo
USER user
WORKDIR /home/user
RUN mkdir -p $HOME/.vnc && (echo 'Gladbeck' | vncpasswd -f > $HOME/.vnc/passwd) && chmod 600 $HOME/.vnc/passwd
EXPOSE 8080
ENTRYPOINT USER=user vncserver -depth 16 -geometry 1280x700 :0 && /opt/novnc/utils/launch.sh --listen 8080
#ENTRYPOINT Xtightvnc :0 -desktop X -auth $HOME/.Xauthority -geometry 1280x730 -depth 16 -rfbwait 120000 -rfbauth $HOME/.vnc/passwd -rfbport 5900 -fp /usr/share/fonts/X11/misc/,/usr/share/fonts/X11/Type1/,/usr/share/fonts/X11/75dpi/,/usr/share/fonts/X11/100dpi/ -co /etc/X11/rgb
